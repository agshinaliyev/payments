package az.ingress.payments;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.kafka.support.SendResult;

import java.util.concurrent.CompletableFuture;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
@EnableKafka
public class PaymentsApplication implements CommandLineRunner {


    private final KafkaTemplate<String, String> kafkaTemplate;

    public static void main(String[] args) {
        SpringApplication.run(PaymentsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//
//        Thread.sleep(1000);
//        long current = System.currentTimeMillis();
//        kafkaTemplate.setProducerListener(new ProducerListener<String, String>() {
//            @Override
//            public void onSuccess(ProducerRecord<String, String> producerRecord, RecordMetadata recordMetadata) {
//                log.info("Message written into kafka {} {}", producerRecord, recordMetadata);
//            }
//        });
//        try {
//
//
//            SendResult<String, String> send = kafkaTemplate.send("user-activation-events", "key", "Hello KAFKA").get();
//            kafkaTemplate.send("user-activation-events", "key", "Hello KAFKA");
//            System.out.println("Sending result data ");
//        } catch (Exception e) {
//
//
//            log.error("Failed to write,retrying");
//
//        }
//        System.out.println("Elapsed time : " + (System.currentTimeMillis() - current));
    }
}
