package az.ingress.payments.controller;

import az.ingress.payments.dto.AccessToken;
import az.ingress.payments.dto.SignInRequest;
import az.ingress.payments.dto.SignInResponse;
import az.ingress.payments.dto.UserRequest;
import az.ingress.payments.security.JwtService;
import az.ingress.payments.service.LoginService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final LoginService loginService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;

//    @GetMapping("/sign-in")
    public String signIn(Authentication authentication) {
        log.info("Authentication : " + authentication.getPrincipal());
        log.info("Authentication :  " + SecurityContextHolder.getContext().getAuthentication().getPrincipal());

//        UsernamePasswordAuthenticationToken u
//                = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),
//                authentication.getCredentials(),authentication.getAuthorities());
//
//        Authentication authenticate = authenticationManagerBuilder.getObject().authenticate(u);
//        SecurityContextHolder.getContext().setAuthentication(authenticate);
//        String token = jwtService.issueToken(authenticate);
//        log.info("--------");

        return "Hello world,user is " + authentication.getPrincipal();
//                + "and token is " + new AccessToken(token);
    }
    
    @PostMapping("/sign-in")
    public ResponseEntity<SignInResponse> signIn(@RequestBody @Valid SignInRequest body){

            return ResponseEntity.ok(loginService.signIn(body));

    }


}
