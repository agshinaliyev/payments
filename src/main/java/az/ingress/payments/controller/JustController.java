package az.ingress.payments.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class JustController {
    @GetMapping("/register")
    public String getRegisterPage(){
        return "register_page";


    }

    @GetMapping("/login")
    public String getLoginPage(){
        return "login_page";


    }
}
