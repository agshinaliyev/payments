package az.ingress.payments.controller;

import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publish")
@RequiredArgsConstructor
public class ProducerController {
    private final KafkaTemplate<String,String> kafkaTemplate;

    @PostMapping("/{message}")
    public ResponseEntity<Void> publish(@PathVariable String message){
        kafkaTemplate.send("sms-event-topic","1",message);
        return ResponseEntity.ok().build();

    }
}
