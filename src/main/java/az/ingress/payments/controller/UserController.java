package az.ingress.payments.controller;

import az.ingress.payments.dto.UserRequest;
import az.ingress.payments.dto.UserResponse;
import az.ingress.payments.service.UserServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Validated
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/register")
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody  UserRequest request) {
        return ResponseEntity.ok(userService.createUser(request));

    }

    @GetMapping("/get/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable Long id) {

        return ResponseEntity.ok(userService.getUser(id));
    }

    @GetMapping("/list")
    public ResponseEntity<List<UserResponse>> listUser(@RequestParam(defaultValue = "0" ) Integer page,
                                                       @RequestParam(defaultValue = "10") Integer size) {

        return ResponseEntity.ok(userService.listUsers(page,size));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<UserResponse> updateUser(@PathVariable Long id,
                                                   @RequestBody UserRequest body) {

        return ResponseEntity.ok(userService.updateUser(id, body));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        userService.deleteUser(id);

        return ResponseEntity.noContent().build();
    }


}
