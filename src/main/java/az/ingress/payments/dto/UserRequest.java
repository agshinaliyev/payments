package az.ingress.payments.dto;


import az.ingress.payments.entity.Authority;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class UserRequest {

    String username;
    String firstName;
    String lastName;
    @NotBlank
    @NotNull
    String email;
    @Size(min = 6, max = 20)
    String password;
    String phoneNumber;

}
