package az.ingress.payments.dto;

import az.ingress.payments.entity.Authority;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class UserResponse {

    Long id;
    String username;
    String firstName;
    String lastName;
    String email;
    String password;
    String phoneNumber;
    List<Authority> authorities;

}
