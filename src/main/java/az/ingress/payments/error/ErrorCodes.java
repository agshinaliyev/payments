package az.ingress.payments.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ErrorCodes implements Errors {


    USER_NOT_FOUND ( "INTERNAL_SERVER_ERROR","user is not present"),
    PASSWORD_VIOLATION_ERROR("INVALID_PASSWORD_TYPE","password size is not correct"),
    EMAIL_ALREADY_EXISTS("REGISTERING_HAS_FAILED","Email has already been registered"),
    USERNAME_ALREADY_EXISTS("REGISTERING_HAS_FAILED","Username has already been registered");
    public final String key;
    public final String message;

    @Override
    public String key() {
        return "";
    }

    @Override
    public String message() {
        return "";
    }
}
