package az.ingress.payments.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponseDto {

    int status;
    public String title;
    public String details;
    @Builder.Default
    Map<String, Object> data =new HashMap<>();


}
