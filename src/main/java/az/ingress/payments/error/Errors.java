package az.ingress.payments.error;

public interface Errors {

    String key();
    String message();

}
