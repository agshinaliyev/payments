package az.ingress.payments.error;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static az.ingress.payments.error.ErrorCodes.USER_NOT_FOUND;
import static az.ingress.payments.error.HttpResponseConstants.*;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class GlobalExceptionHandler extends RuntimeException {

    private final DefaultErrorAttributes def;
    private final MessageSource messageSource;

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ErrorResponseDto> handler(ApplicationException ex, WebRequest request) {
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(USER_NOT_FOUND.)
                .build());
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, Object>> handle(MethodArgumentNotValidException ex, WebRequest request) {
        ex.printStackTrace();

        List<ConstraintViolationError> violationErrors = ex.getBindingResult().getFieldErrors()
                .stream()
                .map(fieldError -> new ConstraintViolationError(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());

        return ofType(request, HttpStatus.BAD_REQUEST, getLocalizedMessage(ex), violationErrors);


    }

//    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, ApplicationException ex) {
//        Locale locale = LocaleContextHolder.getLocale();
//        return ofType(request, status, ex.getLocalizedMessage(locale, messageSource), Collections.EMPTY_LIST);
//
//    }


    private ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status,
                                                       String message, List validationErrors) {

        Map<String, Object> attributes = def.getErrorAttributes(request, ErrorAttributeOptions.defaults());
        attributes.put(STATUS, status.value());
        attributes.put(ERROR, getLocalizedReasonPhrase(status));
        attributes.put(MESSAGE, message);
        attributes.put(ERRORS, validationErrors);
        attributes.put(PATH, ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(attributes, status);


    }

    private String getLocalizedMessage(Exception ex) {

        Locale locale = LocaleContextHolder.getLocale();
        var key = ex.getClass().getName() + ".message";
        try {
            return messageSource.getMessage(key, new Object[]{}, locale);

        } catch (NoSuchMessageException exception) {

            log.warn("Consider adding localized message for key {} and locale {}", key, locale);

        }
        return ex.getMessage();

    }

    private String getLocalizedReasonPhrase(HttpStatus status) {

        Locale locale = LocaleContextHolder.getLocale();
        try {
            return messageSource.getMessage(status.value() + ".message", new Object[]{}, locale);

        } catch (NoSuchMessageException exception) {

            log.warn("Consider adding localized message for key {} and locale {}", status.value(), locale);
        }

        return status.getReasonPhrase();


    }




}
