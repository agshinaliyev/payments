package az.ingress.payments.security;

import az.ingress.payments.entity.Authority;
import az.ingress.payments.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.awt.*;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static io.jsonwebtoken.SignatureAlgorithm.forSigningKey;

@Service
@RequiredArgsConstructor
public class JwtService {
    @Value("${spring.security.secretKey}")
    private static SecretKey secretKey;
    @Value("${spring.security.expDate}")
    private long expDate;


    public String issueToken(Authentication authentication) {
        secretKey = Jwts.SIG.HS512.key().build();
        final JwtBuilder jwtBuilder = Jwts.builder()
                .header().add(Map.of("type", "JWT")).and()
                .subject(authentication.getName())
                .signWith(secretKey)
                .claims(Map.of("role", authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList())))
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plusSeconds(expDate)));

        return jwtBuilder.compact();


    }

    public Claims parseToken(String token) {

        return Jwts.parser().verifyWith(secretKey)
                .build()
                .parseSignedClaims(token).getPayload();


    }


}
