package az.ingress.payments.security;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TokenAuthService implements AuthService {
    private static final String BEARER = "Bearer ";
    private static final String AUTHORITY_CLAIM = "authority";
    private final JwtService jwtService;
    private final ModelMapper modelMapper;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader("Authorization"))
                .filter(this::isBearerAuth)
                .flatMap(this::getAuthenticationBearer);

    }

    @Override
    public boolean isBearerAuth(String header) {
        return header.toLowerCase().startsWith("bearer");
    }

    @Override
    public Optional<Authentication> getAuthenticationBearer(String header) {
        String token = header.substring(BEARER.length()).trim();
        Claims claims = jwtService.parseToken(token);
        return Optional.of(getAuthenticationBearer(claims));
    }

    @Override
    public Authentication getAuthenticationBearer(Claims claims) {
        var roles = List.of(claims.get(AUTHORITY_CLAIM, String.class));
        var authorityList = roles.stream().map(SimpleGrantedAuthority::new).toList();
        JwtCredentials credentials = modelMapper.map(claims, JwtCredentials.class);
        User user =new User(credentials.getSubject(),"",authorityList);

        return new UsernamePasswordAuthenticationToken(user,credentials,authorityList);

    }
}
