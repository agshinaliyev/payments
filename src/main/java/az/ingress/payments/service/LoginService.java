package az.ingress.payments.service;

import az.ingress.payments.dto.SignInRequest;
import az.ingress.payments.dto.SignInResponse;
import org.springframework.stereotype.Service;

public interface LoginService {


    SignInResponse signIn(SignInRequest body);
}
