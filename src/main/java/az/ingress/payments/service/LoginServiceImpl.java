package az.ingress.payments.service;

import az.ingress.payments.dto.SignInRequest;
import az.ingress.payments.dto.SignInResponse;
import az.ingress.payments.security.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;
    @Override
    public SignInResponse signIn(SignInRequest body) {

        log.info("Authentication requested by user {}",body.getUsername());

        UsernamePasswordAuthenticationToken upa = new UsernamePasswordAuthenticationToken(body.getUsername(),body.getPassword());
        Authentication authenticate = authenticationManagerBuilder.getObject().authenticate(upa);
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        log.info("sign in result is {}",authenticate);
        String accessToken = jwtService.issueToken(authenticate);
        SignInResponse response = SignInResponse.builder()
                .accessToken(accessToken)
                .build();
        return response;


    }
}
