package az.ingress.payments.service;

import az.ingress.payments.dto.UserRequest;
import az.ingress.payments.dto.UserResponse;

import java.util.List;

public interface UserService {
    UserResponse createUser(UserRequest request);

    UserResponse getUser(Long id);

    List<UserResponse> listUsers(Integer page,Integer size);

    UserResponse updateUser(Long id, UserRequest body);

    void deleteUser(Long id);


}
