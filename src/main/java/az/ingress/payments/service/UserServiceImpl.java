package az.ingress.payments.service;

import az.ingress.payments.dto.UserRequest;
import az.ingress.payments.dto.UserResponse;
import az.ingress.payments.entity.Authority;
import az.ingress.payments.entity.User;
import az.ingress.payments.entity.UserAuthority;
import az.ingress.payments.error.ApplicationException;
import az.ingress.payments.repository.AuthorityRepository;
import az.ingress.payments.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static az.ingress.payments.error.ErrorCodes.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public UserResponse createUser(UserRequest request) {


        User user = modelMapper.map(request, User.class);
        if (userRepository.findByEmail(user.getEmail()).isPresent())
            throw new ApplicationException(EMAIL_ALREADY_EXISTS);
        if (userRepository.findByUsername(user.getUsername()).isPresent())
            throw new ApplicationException(USERNAME_ALREADY_EXISTS);
        Authority authority = new Authority();
        authority.setAuthority(UserAuthority.USER);
        authorityRepository.save(authority);
        user.setAuthorities(List.of(authority));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        if (user.getPassword().length() < 6) {
            throw new ApplicationException(PASSWORD_VIOLATION_ERROR);
        }
        return modelMapper.map(userRepository.save(user), UserResponse.class);
    }

    @Override
    public UserResponse getUser(Long id) {


        return modelMapper.map(validateAndGet(id), UserResponse.class);
    }

    @Override
    public List<UserResponse> listUsers(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        return userRepository.findAll(pageRequest)
                .stream()
                .map((user -> modelMapper.map(userRepository.save(user), UserResponse.class)))
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse updateUser(Long id, UserRequest body) {
        validateAndGet(id);
        User user = modelMapper.map(body, User.class);
        user.setId(id);
        return modelMapper.map(userRepository.save(user), UserResponse.class);
    }

    @Override
    public void deleteUser(Long id) {

        userRepository.delete(validateAndGet(id));


    }

    private User validateAndGet(Long id) {

        return userRepository.findById(id).orElseThrow(() -> new ApplicationException(USER_NOT_FOUND));

    }
}
