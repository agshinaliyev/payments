package az.ingress.payments.threads;

import jakarta.annotation.security.RunAs;
import lombok.RequiredArgsConstructor;

public class Main {

    private final Student student =new Student();


    public static void main(String[] args) throws InterruptedException {
        Main main =new Main();

        Runnable runnable1 = () ->  main.populateObject(1L,"Agshin","Aliyev");
        Runnable runnable2 = () ->  main.populateObject(2L,"None","None");
        Runnable runnable3 = () ->  main.populateObject(3L,"Some","Some");
        Thread thread1=new Thread(runnable1);
        Thread thread2=new Thread(runnable2);
        Thread thread3=new Thread(runnable3);



        thread1.start();
//        Thread.sleep(1000);
        thread2.start();
//        Thread.sleep(1000);
        thread3.start();



    }

    private void populateObject(Long id,String firstName,String lastName){


        student.setId(id);
        student.setFirstName(firstName);
        student.setLastName(lastName);
        saveObjects(student);



    }

    private void saveObjects(Student student){

        System.out.println("saved objects are : " + student);


    }




}
